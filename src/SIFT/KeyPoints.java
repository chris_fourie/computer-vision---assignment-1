package SIFT;

import utilities.TupleExtrema;
import utilities.TupleKeyPoint;

public class KeyPoints {
        public TupleKeyPoint[] keyPoints;

        //FIXME resolve null pointer errors - problem is that we don't know how many extrema / keypoints we will have so how to we compare variable length arrays? perhaps just set all other values to 0?
        //TODO find key points where all the overlapping blurs are
    public KeyPoints(TupleExtrema[][] octaveExtrema)
    {


        int numOctaves = octaveExtrema.length;
        int count = 0;
        this.keyPoints = new TupleKeyPoint[octaveExtrema[0].length];

        // IF exterma is also extrema in at least 1 other octave THEN make it a key point
        // use negative test - that is, if
        for (int oct = 0; oct <numOctaves-1; oct++) {

            int numExtrema = octaveExtrema[oct+1].length; //assuming the lower octave has fewer extrema
            for (int j = 0; j < numExtrema-1 ; j++) {
                int x1 = octaveExtrema[oct][j].x;
                int y1 = octaveExtrema[oct][j].y;

                for (int k = 0; k < numExtrema-1; k++) {
                    try{
                    int x2 = octaveExtrema[oct+1][k].x*2; //scale up coordinates to check match
                    int y2 = octaveExtrema[oct+1][k].y*2; // scale up coordinates to check match

                    if ((x1 == x2 || x1 == x2-1 || x1 == x2+1) && (y1 == y2 || y1 == y2-1 || y1 == y2+1) )
                    {
                        if (oct == 0){
                            this.keyPoints[count] = new TupleKeyPoint(x1,y1);
                            count ++;
                        }
                        else{
                            this.keyPoints[count] = new TupleKeyPoint(x1*oct*2 , y1*oct*2);
                        }
                    }
                    } catch (NullPointerException npe){} // need to do this as we cannot determine how many extrema or keypoints there will be
                }

            }


        }


    }


}
