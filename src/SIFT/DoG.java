package SIFT;

import java.io.IOException;

public class DoG {

    public static int[][][] calc(Octave inputOct) throws IOException {
        int numBlur = inputOct.numBlur; //6
         //TODO turn these 4 arrays into 3 d arrays -> 1 per octave
            //TODO assign the array here - should also be able to get heights?
            int width = inputOct.width;
            int height = inputOct.height;
            int [][][] inputImgs = new int [numBlur][width][height];
            int [][][] outputImgs = new int [numBlur][width][height];

            for (int blur = 0; blur < numBlur-1; blur++) {
                inputImgs[blur] = inputOct.data[blur];
                inputImgs[blur+1] = inputOct.data[blur+1];

                if (blur<numBlur-1) {
                        for (int i = 0; i < width; i++) {
                            for (int j = 0; j < height; j++) {
                                outputImgs[blur][i][j] = inputImgs[blur+1][i][j] - inputImgs[blur][i][j]; //TODO why is img2 all 0?? //TODO causes last image to be blank cause e.g. can't subtrate the last
                            }
                        }
                }
            }
            return outputImgs;
    }
}
