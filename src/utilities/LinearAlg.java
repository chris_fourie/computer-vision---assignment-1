package utilities;

public class LinearAlg {

    public static int[][] affineTransformAndStitch (double[][] H, int[][] imgArrSrc, int[][] imgArrDst) //this needs a double H array
    {
        int widthSrc = imgArrSrc.length;
        int heightSrc = imgArrSrc[0].length;
        int widthDst = imgArrDst.length;
        int heightDst = imgArrDst[0].length;

        //CANVAS
        int canvasWidth = widthSrc + widthDst;
        int canvasHeight = heightSrc + heightDst;

        int [][] outputArr = new int[canvasWidth][canvasHeight]; //make large enough canvas
        int xOffset = canvasWidth/4 ;
        int yOffset = canvasHeight/4;
        double s = 0.05; //FIXME scalar -> to mitigate the effect of the int rounding from the findHomography function

        //Draw fist image (Destination)
        for (int x = 0; x < widthDst; x++) {
            for (int y = 0; y < heightDst ; y++) {
                outputArr[xOffset + x][yOffset + y] = imgArrDst[x][y];
            }
        }

        //Draw second image (Source) -> i.e. placing Source in Destination
        for (int x = 0; x < widthSrc; x++) {
            for (int y = 0; y < heightSrc ; y++) {
//                System.out.println("H0: " + H[0][0] + " " + H[0][1] + " " + H[0][2]);
//                System.out.println("H1: " + H[1][0] + " " + H[1][1] + " " + H[1][2]);
//                System.out.println("H2: " + H[2][0] + " " + H[2][1] + " " + H[2][2]);
                //VectorOut = MatrixIn x VectorIn
                double [] pointVecOut = new double[] {
                        x*H[0][0]*s + y*H[0][1]*s + 1*H[0][2],  //x
                        x*H[1][0]*s + y*H[1][1]*s + 1*H[1][2],  //y
                        x*H[2][0]*s + y*H[2][1]*s + 1*H[2][2],  //1
                };
                try{
//                    System.out.println("new x: " + ((pointVecOut[0]) + " y: " + (pointVecOut[1])+ " 1: " + (pointVecOut[2])));
                    outputArr[xOffset + x + (int)pointVecOut[0]][yOffset + y + (int)pointVecOut[1]] = imgArrSrc[x][y]; //FIXME not correctly translating OR is it the H matrix?
                }catch (ArrayIndexOutOfBoundsException e){}
            }
        }
        return outputArr;
    }

    public static double[][] intToDouble(int[][] source) {
        int width = source.length;
        int height = source[0].length;

        double[][] dest = new double[width][height];
        for(int col=0; col<width; col++) {
            for (int row = 0; row < height ; row++) {
                dest[col][row] = source[col][row];
            }
        }
        return dest;
    }



}
