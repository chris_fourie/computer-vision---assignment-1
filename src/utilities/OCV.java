package utilities;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

import static org.opencv.imgproc.Imgproc.cvtColor;

public class OCV {
    public static void main(String args[]) throws IOException {
        Mat imgMat = imageToMatrix("C:\\Users\\Testing\\Desktop\\Repos\\computer-vision\\Assignment_1-SIFT-RANSAC\\CV_Assignment_1\\src\\utilities\\test.jpg");

        System.out.println(imgMat);
        System.out.print("R: "+ imgMat.get(2,3)[0]); //returns 3 double arrays => RGB
        System.out.print(" G: " + imgMat.get(2,3)[1]);
        System.out.print(" B" + imgMat.get(2,3)[2]);

        Mat imgMatGray = new Mat();
        cvtColor( imgMat, imgMatGray, Imgproc.COLOR_RGB2GRAY );

        BufferedImage bImg = matrixToBufferedImage(imgMatGray);

        Image_Helper.displayImage(bImg);

    }

    public static Mat imageToMatrix (String filename) {
        //Loading the OpenCV core library
        System.loadLibrary( Core.NATIVE_LIBRARY_NAME );

        //Instantiating the Imagecodecs class
        Imgcodecs imageCodecs = new Imgcodecs();

        //Reading the Image from the file
        Mat matrix = imageCodecs.imread(filename);

        System.out.println("Image Loaded");
        return matrix;
    }


    public static BufferedImage matrixToBufferedImage(Mat m){
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if ( m.channels() > 1 ) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels()*m.cols()*m.rows();
//        byte [] b = new byte[bufferSize];
        int[] b = new int[bufferSize];
        m.get(0,0,b); // get all the pixels

        ByteBuffer byteBuffer = ByteBuffer.allocate(b.length * 4);
        IntBuffer buffer = byteBuffer
                .asIntBuffer()
                .put(b);

        byte[] source = byteBuffer.array();

        BufferedImage img = new BufferedImage(m.cols(),m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
        System.arraycopy(source, 0, targetPixels, 0, b.length);

        return img;
    }

    public static int[][] matrixToArray(Mat matrix) {
        int width = matrix.cols();
        int height = matrix.rows();

        int[][] arr = new int[width][height];

        for (int col = 0; col < width ; col++) {
            for (int row = 0; row < height; row++) {
                arr[col][row] = (int)matrix.get(row,col)[0];
            }
        }
        return arr;
    }

    public static double[][] matrixToArrayDouble(Mat matrix) {
        int width = matrix.cols();
        int height = matrix.rows();

        double [][] arr = new double[width][height];

        for (int col = 0; col < width ; col++) {
            for (int row = 0; row < height; row++) {
                arr[col][row] = (int)matrix.get(row,col)[0];
            }
        }
        return arr;
    }

    public static Mat arrayToMatrix (int [][] inputArr) {
        int width = inputArr[0].length;
        int height = inputArr.length;
        Mat outputMat = new Mat(height, width, CvType.CV_32S ); //make sure with and height are right // previously CvType.CV_32S  // CvType.CV_8U also doesn't work

        int [] inputArr1D = Image_Helper.twoDArray_To_oneDArray(inputArr);
        int row = 0, col = 0;
        outputMat.put( row, col, inputArr1D);

    return outputMat;
    }


    //TODO Add drawing of key points and matches //https://docs.opencv.org/java/2.4.2/index.html?org/opencv/features2d/Features2d.html


}
